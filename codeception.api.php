<?php

/**
 * @file
 * Describe hooks provided by the Views module.
 */

/**
 * @defgroup codeception_hooks Codeception Module Hooks
 * @{
 * Hooks to enable modules to declare Codeception projects for integration
 * with the Codeception module.
 */

/**
 * Describes a Codeception project to the Codeception module.
 *
 * @return array
 *   An array whose keys must be unique and whose values is an associative
 *   array containing properties of the Codeception project:
 *   - key: unique key for the project.
 *   - name: human readable title for the project.
 *   - path: Drupal path to the project folder (normally within the custom
 *     module.
 */
function hook_codeception_project() {
  return array(
    'cc_project' => array(
      'key' => 'cc_project',
      'name' => 'Codeception Example Project',
      'path' => drupal_get_path('module', 'cc_project') . '/codeception',
    ),
  );
}

/**
 * @}
 */

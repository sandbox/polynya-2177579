Codeception for Drupal 7.x
==========================

Description
===========
* Provides integration with the Codeception test framework.
* Allows Codeception tests to be run through the Drupal UI and Drush.
* The advantages over standard use of Codeception include:
* Run tests through the Drupal UI on environments where the operator does not
have permissions to access the command line.
* Use Drupal code within Codeception testcases, for example, to set test
parameters using Drupal variables or EntityFieldQuery.


Installation
============
* Follow the Codeception guide to install Codeception via Composer, or use one of the Drupal Composer modules.
* Enable this module
* Create a custom module.
* Create a Codeception project inside.
* Use hook_codeception_project to declare the location of the project.

See the example module cc_project.

Configuration
=============
admin/config/codeception

Usage
=====
admin/reports/codeception

Drush
=====
drush codeception run <PROJECT> <SUITE> <FOLDER> <TESTCASE>


Settings via variables
======================
To follow.

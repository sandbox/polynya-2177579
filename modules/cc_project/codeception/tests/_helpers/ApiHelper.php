<?php

/**
 * @file
 * ApiHelper
 */

namespace Codeception\Module;

/**
 * Class ApiHelper
 *
 * Define some custom functions for ApiGuy.
 *
 * @package Codeception\Module
 */
class ApiHelper extends \Codeception\Module {

  /**
   * Check if REST response satisfies an Xpath query.
   *
   * @param string $query
   *   Xpath query
   */
  public function seeResponseContainsDOMXpath($query = '') {

    if ($response = $this->getModule('REST')->response) {
      $dom = new \DOMDocument();
      $dom->loadXML($response);
      $xpath = new \DOMXPath($dom);

      $root = $dom->documentElement;
      if ($root->hasAttribute('xmlns')) {
        $ns = $root->getAttribute('xmlns');
        $xpath->registerNameSpace('dummy', $ns);
      }
      $result = $xpath->query($query);
    }
    else {
      $result = FALSE;
    }
    \PHPUnit_Framework_Assert::assertTrue($result && $result->length > 0, 'No elements were found for this DOM xpath selector');
  }

  /**
   * Check if REST response satisfies an Xpath query.
   *
   * @param string $query
   *   Xpath query
   */
  public function grabFirstResultFromDOMXpath($query = '') {

    if ($response = $this->getModule('REST')->response) {
      $dom = new \DOMDocument();
      $dom->loadXML($response);
      $xpath = new \DOMXPath($dom);

      $root = $dom->documentElement;
      if ($root->hasAttribute('xmlns')) {
        $ns = $root->getAttribute('xmlns');
        $xpath->registerNameSpace('dummy', $ns);
      }
      $result = $xpath->query($query);
      $value = $result->item(0)->nodeValue;
      return $value;
    }
    else {
      return;
    }
  }

  /**
   * Check if REST response validates against a schema.
   *
   * @param string $schema
   *   URL or path to an XML schema.
   */
  public function seeResponseContainsValidatedXML($schema) {

    if ($response = $this->getModule('REST')->response) {
      $dom = new \DOMDocument();
      $dom->loadXML($response);
      $result = $dom->schemaValidate($schema);
    }
    else {
      $result = FALSE;
    }
    \PHPUnit_Framework_Assert::assertTrue($result, 'XML response does not validate.');
  }

  /**
   * Write the REST response to a file.
   *
   * @param string $filename
   *   Full path to a file.
   */
  public function seeResponseIsWrittenToFile($filename) {

    $response = $this->getModule('REST')->response;
    $result = file_put_contents($filename, $response, LOCK_EX);

    \PHPUnit_Framework_Assert::assertFileExists($filename, 'File does not exist.');
    \PHPUnit_Framework_Assert::assertEquals(strlen($response), $result, 'File length does not match the XML response length.');
  }

  /**
   * See if response status code matches the provided code.
   *
   * @param int $code
   *   Status code
   */
  public function seeResponseStatusCodeIs($code) {
    $status = $this->getModule('REST')->client->getInternalResponse()->getStatus();

    $this->assertEquals($code, $status);

    \PHPUnit_Framework_Assert::assertEquals($code, $status, 'Status code ' . $code . ' does not match ' . $status);
  }

  /**
   * Test if actual value is greater than expected value.
   *
   * @param mixed $expected
   *   Expected value
   * @param mixed $actual
   *   Actual value
   * @param string $message
   *   Message
   */
  public function assertGreaterThan($expected, $actual, $message) {
    $this->assertGreaterThen($expected, (float) $actual, $message);
  }

}

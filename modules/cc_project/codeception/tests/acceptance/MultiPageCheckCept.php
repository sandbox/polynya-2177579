<?php

/**
 * @file
 * Check all pages.
 */

// Scenario init.
$scenario->group('Multi-page-check');

$i = new WebGuy($scenario);
$i->wantTo('test some elements on all pages');

$entity_query = new EntityFieldQuery();
$entity_query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'page')
    ->propertyCondition('status', 1);
if ($limit = variable_get('cc_project_query_limit', 10)) {
  $entity_query->range(0, $limit);
}
$result = $entity_query->execute();
if (drupal_is_cli()) {
  drush_print(t('testing !count nodes', array('!count' => count($result['node']))));
}
foreach (array_keys($result['node']) as $nid) {
  $i->amGoingTo('visit the page node/' . $nid);
  $i->amOnPage('node/' . $nid);
  $i->amGoingTo('check there is an h1 element');
  $i->seeElement('h1');
}

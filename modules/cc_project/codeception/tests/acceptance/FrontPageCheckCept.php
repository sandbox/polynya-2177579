<?php

/**
 * @file
 * Some front page checks.
 */

// Scenario init.
$scenario->group('Front-page');

$i = new WebGuy($scenario);
$i->wantTo('test some elements on the front page');

// Scenario steps start.
$i->amGoingTo('visit the front page');
$i->amOnPage('/');

$i->amGoingTo('check there is an h1 element');
$i->seeElement('h1');

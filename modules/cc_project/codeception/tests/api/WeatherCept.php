<?php

/**
 * @file
 * Test weather api
 */

// Scenario init.
$scenario->group('Weather');
$i = new ApiGuy($scenario);
$i->wantTo('test the weather API');

// Scenario steps start.
$i->amGoingTo('call the weather REST service');
$rest_url = variable_get('cc_project_rest_url', 'http://aviationweather.gov/adds/dataserver');
$resource = 'httpparam';
$data = array(
  'dataSource' => 'metars',
  'requestType' => 'retrieve',
  'format' => 'xml',
  'stationString' => 'KDEN',
  'hoursBeforeNow' => 2,
);
$i->sendGET($resource, $data);

// Tests.
$i->seeResponseStatusCodeIs(200);
$i->seeResponseIsXml();
$i->seeResponseIsWrittenToFile('response.xml');
$i->seeResponseContainsDOMXpath('/response/data_source');
$i->seeResponseContainsDOMXPath('/response/data/METAR/temp_c');
$temp = $i->grabFirstResultFromDOMXpath('/response/data/METAR/temp_c');
$i->assertGreaterThan(0, (string) $temp, 'temperature is above freezing');

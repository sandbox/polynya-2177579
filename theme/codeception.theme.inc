<?php

/**
 * @file
 * Codeception theme functions.
 */

/**
 * Returns HTML for a project report.
 *
 * @param array $variables
 *   An associative array containing:
 *   - project: array contain key, name, and path.
 *
 * @ingroup themeable
 */
function theme_codeception_project_report($variables) {
  $project = $variables['project'];
  $errors = array();
  $failures = array();

  $output = '<h2>Project - ' . $project['name'] . '</h2>';

  $replace = array('!project' => urlencode($project['key']));

  $project_path = _codeception_get_project_path($project);

  $log_path = $project_path . '/tests/_log';
  $fakepath_root = format_string(CODECEPTION_REPORTS_PATH . '/!project/report/', $replace);
  $data = array();
  $report_types = array('html', 'xml', 'tap.log', 'json');
  foreach ($report_types as $report_type) {
    $replace['@type'] = $report_type;
    $file_realpath = $log_path . '/report.' . $report_type;
    if (file_exists($file_realpath)) {
      $reports[$report_type]['real'] = $file_realpath;
      $reports[$report_type]['fake'] = $fakepath_root . $report_type;
      if (!isset($report_created)) {
        $report_created = filemtime($file_realpath);
        $replace['@date'] = date('j M Y H:i:s', $report_created);
        $data[] = t('Last run: @date', $replace);
      }
      $data[] = l(t('@type report', $replace), $fakepath_root . $report_type);
    }
  }

  if ($data) {
    $items = array(
      'title' => t('Latest Codeception report'),
      'items' => $data,
    );
    $output .= theme('item_list', $items);

    if ($report_xml_realpath = $reports['xml']['real']) {

      $contents = file_get_contents($report_xml_realpath);
      $dom = new DOMDocument();
      $dom->loadXML($contents);

      $testsuites = $dom->getElementsByTagName('testsuite');
      foreach ($testsuites as $testsuite) {
        $data = array();
        $replace = array(
          '@date' => date('j M Y H:i:s', $report_created),
          '@tests' => $testsuite->getAttribute('tests'),
          '@assertions' => $testsuite->getAttribute('assertions'),
          '@failures' => $testsuite->getAttribute('failures'),
          '@errors' => $testsuite->getAttribute('errors'),
        );
        $data[] = t('Tests: @tests', $replace);
        $data[] = t('Assertions: @assertions', $replace);
        $data[] = t('Failures: @failures', $replace);
        $data[] = t('Errors: @errors', $replace);

        $testcases = $testsuite->getElementsByTagName('testcase');
        foreach ($testcases as $testcase) {
          $replace = array(
            '!suite' => $testsuite->getAttribute('name'),
            '!file' => $testcase->getAttribute('file'),
            '!name' => $testcase->getAttribute('name'),
            '!a' => $testcase->getAttribute('assertions'),
            '!time' => sprintf("%0.2f", $testcase->getAttribute('time')),
          );
          $data[] = t('!name - !a assertions, !time secs', $replace);
          $items = array(
            'title' => t('Tests - !suite suite', $replace),
            'items' => $data,
          );
        }
        $testsuite_errors = $testsuite->getElementsByTagName('error');
        foreach ($testsuite_errors as $testsuite_error) {
          $errors[] = $testsuite_error->getAttribute('type') . ' - ' . $testsuite_error->nodeValue;
        }
        $testsuite_failures = $testsuite->getElementsByTagName('failure');
        foreach ($testsuite_failures as $testsuite_failure) {
          $failures[] = $testsuite_failure->getAttribute('type') . ' - ' . $testsuite_failure->nodeValue;
        }

        $output .= theme('item_list', $items);

        if ($errors) {
          $items = array(
            'title' => t('Errors'),
            'items' => $errors,
          );
          if ($data) {
            $output .= theme('item_list', $items);
          }
        }
        if ($failures) {
          $items = array(
            'title' => t('Failures'),
            'items' => $failures,
          );
          if ($data) {
            $output .= theme('item_list', $items);
          }
        }
      }
    }
  }
  return $output;
}

/**
 * Returns HTML for a project's actions, for example, to run tests.
 *
 * @param array $variables
 *   An associative array containing:
 *   - project: array contain key, name, and path.
 *
 * @ingroup themeable
 */
function theme_codeception_project_actions($variables) {
  $project = $variables['project'];
  $output = '';
  $map = _codeception_get_tests_map($project);

  $data = array();
  foreach ($map->suites as $suite) {
    $replace = array(
      '!suite' => $suite,
      '!suite_arg' => urlencode($suite),
      '!project' => urlencode($project['key']),
    );

    $run_path = CODECEPTION_REPORTS_PATH . format_string('/!project/run/!suite_arg', $replace);
    $data[] = l(t('run !suite', $replace), $run_path);
  }
  if ($data) {
    $items = array(
      'title' => t('Run Codeception tests - suites'),
      'items' => $data,
    );
    $output .= theme('item_list', $items);
  }

  $data = array();
  foreach ($map->folders as $suite_name => $suite) {
    foreach ($suite as $folder) {
      $replace = array(
        '!project' => urlencode($project['key']),
        '!suite' => $suite_name,
        '!suite_arg' => urlencode($suite_name),
        '!folder' => $folder,
        '!folder_arg' => urlencode($folder),
      );

      $run_path = CODECEPTION_REPORTS_PATH . format_string('/!project/run/!suite_arg/!folder_arg', $replace);
      $data[] = l(t('run !suite !folder', $replace), $run_path);
    }
  }
  if ($data) {
    $items = array(
      'title' => t('Run Codeception tests - folders'),
      'items' => $data,
    );
    $output .= theme('item_list', $items);
  }

  $data = array();
  foreach ($map->testcases as $suite_name => $suite) {
    foreach ($suite as $testcase) {
      $replace = array(
        '!project' => urlencode($project['key']),
        '!suite' => $suite_name,
        '!suite_arg' => urlencode($suite_name),
        '!testcase' => $testcase,
        '!testcase_arg' => urlencode($testcase),
      );

      $run_path = CODECEPTION_REPORTS_PATH . format_string('/!project/run/!suite_arg/!testcase_arg', $replace);
      $data[] = l(t('run !suite !testcase', $replace), $run_path);
    }
  }
  if ($data) {
    $items = array(
      'title' => t('Run Codeception tests - testcases'),
      'items' => $data,
    );
    $output .= theme('item_list', $items);
  }

  return $output;
}

<?php

/**
 * @file
 * Functions to execute Codeception commands.
 */

/**
 * Execute Codeception 'run' command based on parameters.
 *
 * @param string $project_key
 *   Codeception project key
 * @param string $suite
 *   Codeception suite
 * @param string $folder
 *   Codeception folder
 * @param string $testcase
 *   Codeception testcase
 * @param string $data
 *   Output from the 'execute' method in the Drupal Codeception class.
 *
 * @return string
 *   XML string
 *
 * @todo Finish building the XML string in DrupalExtension and provide a schema.
 */
function _codeception_run($project_key, $suite = NULL, $folder = NULL, $testcase = NULL, $data = NULL) {
  if ($project = codeception_get_codeception_project(urldecode($project_key))) {
    // Build Codeception folder if flag is set or if the folder does not exist.
    if (variable_get('codeception_rebuild_project', TRUE) || !file_exists(_codeception_get_project_path($project))) {
      module_load_include('inc', 'codeception', 'includes/codeception.build_project');
      _codeception_build_project($project);
    }

    if (variable_get('codeception_execute_build', FALSE)) {
      $c_build = new Codeception('build', urldecode($project_key));
      $c_build->setOutputFlag(variable_get('codeception_option_output', TRUE));
      $c_build->execute();
    }

    $c_run = new Codeception('run', urldecode($project_key));
    if ($suite) {
      $c_run->setSuite(urldecode($suite));
    }
    if ($folder) {
      $c_run->setGroup(urldecode($folder));
    }
    if ($testcase) {
      $c_run->setTestcase(urldecode($testcase));
    }
    if ($data) {
      $c_run->setData($data);
    }
    _codeception_set_command_options($c_run);
    return $c_run->execute();
  }
  return t('Project not found.');
}

/**
 * Execute Codeception 'build' command.
 *
 * @param string $project_key
 *   Codeception project key
 *
 * @return string
 *   String
 */
function _codeception_build($project_key) {
  if ($project = codeception_get_codeception_project(urldecode($project_key))) {
    // Build Codeception folder if flag is set or if the folder does not exist.
    if (variable_get('codeception_rebuild_project', TRUE) || !file_exists(_codeception_get_project_path($project))) {
      module_load_include('inc', 'codeception', 'includes/codeception.build_project');
      _codeception_build_project($project);
    }

    $c = new Codeception('build', urldecode($project_key));
    $c->setOutputFlag(variable_get('codeception_option_output', TRUE));
    return $c->execute();
  }
  return t('Project not found.');
}

/**
 * Set command line options for Codeception.
 *
 * @param Symfony\Component\Console\Application $c
 *   Symfony application.
 */
function _codeception_set_command_options(&$c) {
  $c->setOutputFlag(variable_get('codeception_option_output', TRUE));
  $c->setDebugFlag(variable_get('codeception_option_debug', FALSE));
  $c->setStepsFlag(variable_get('codeception_option_steps', FALSE));
  $c->setXMLFlag(variable_get('codeception_option_xml', TRUE));
  $c->setHTMLFlag(variable_get('codeception_option_html', TRUE));
  $c->setTAPFlag(variable_get('codeception_option_tap', TRUE));
  $c->setJSONFlag(variable_get('codeception_option_json', TRUE));
}

<?php

/**
 * @file
 * Private helper functions for Codeception.
 */

/**
 * Retrieve the Composer vendor directory.
 *
 * @return string
 *   Composer vendor directory. Empty string if directory not found.
 */
function _codeception_composer_vendor_dir() {
  // Return variable if it is set.
  if ($vendor_dir = variable_get('codeception_composer_vendor_dir', '')) {
    return $vendor_dir;
  }

  // Return directory from Composer Manager module.
  if (function_exists('composer_manager_vendor_dir')) {
    return composer_manager_vendor_dir();
  }

  // Check the most likely location.
  if (is_dir('sites/all/vendor')) {
    $replace = array(
      '!root' => DRUPAL_ROOT,
      '!sep' => DIRECTORY_SEPARATOR,
    );
    return format_string('!root!sepsites!sepall!sepvendor', $replace);
  }

  // Directory has not been found.
  return '';
}

/**
 * Retrieve the directory of the Codeception code.
 *
 * @return string
 *   Directory of the Codeception code,
 *   usually in the Composer vendor directory.
 */
function _codeception_codeception_dir() {
  // Return variable if it is set.
  if ($codeception_dir = variable_get('codeception_codeception_dir', '')) {
    return $codeception_dir;
  }

  // Look for Codeception within Composer vendor directory.
  if ($vendor_dir = _codeception_composer_vendor_dir()) {
    $replace = array(
      '!sep' => DIRECTORY_SEPARATOR,
      '@vendor' => $vendor_dir,
    );
    $codeception_dir = format_string('@vendor!sepcodeception', $replace);
    if (file_exists($codeception_dir . DIRECTORY_SEPARATOR . 'autoload.php')) {
      return $codeception_dir;
    }
  }
  return '';
}

/**
 * Get map of suites, folders and testcases.
 *
 * Scan Codeception folders for suites
 * and the folders and testcases they contain.
 *
 * @param array $project
 *   Project details - key, name, path
 *
 * @return \stdClass
 *   Fields for suites, folders and testcases.
 */
function _codeception_get_tests_map($project) {
  $path = $project['path'] . '/tests';

  $map = new stdClass();
  $map->suites = _codeception_get_suites($project);

  $map->folders = array();
  $map->testcases = array();

  foreach ($map->suites as $suite) {
    $map->folders[$suite] = _codeception_get_suite_folders($project, $suite);
    $map->testcases[$suite] = _codeception_get_suite_testcases($project, $suite);
  }

  // If no folders were found in a suite then delete the suite.
  foreach ($map->folders as $key => $suite) {
    if (empty($suite)) {
      unset($map->folders[$key]);
    }
  }

  // If no testcases were found in a suite then delete the suite.
  foreach ($map->testcases as $key => $suite) {
    if (empty($suite)) {
      unset($map->testcases[$key]);
      if (isset($map->suites[array_search($key, $map->suites)])) {
        unset($map->suites[array_search($key, $map->suites)]);
      }
    }
  }

  sort($map->suites);
  return $map;
}

/**
 * Get suite names.
 *
 * @param array $project
 *   Project details - key, name, path
 *
 * @return array
 *   Suite names.
 */
function _codeception_get_suites($project) {
  $suites = array();
  $path = $project['path'] . '/tests';

  foreach (new DirectoryIterator($path) as $file_info) {
    if (!$file_info->isDot() && $file_info->isDir()) {
      $name = $file_info->getFilename();
      if (strpos($name, '_') !== 0) {
        $suites[] = $name;
      }
    }
  }
  sort($suites);
  return $suites;
}

/**
 * Get suite folders.
 *
 * Get folder names found inside a suite.
 *
 * @param array $project
 *   Project details - key, name, path
 * @param string $suite
 *   Suite name
 *
 * @return array
 *   Folder names.
 */
function _codeception_get_suite_folders($project, $suite) {
  $folders = array();
  $path = $project['path'] . '/tests/' . $suite;
  $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
  foreach ($objects as $name => $object) {
    if ($object->isDir()) {
      $filename = str_replace($path . '/', '', $name);
      if (substr($filename, -1) != '.') {
        $folders[] = $filename;
      }
    }
  }
  sort($folders);
  return $folders;
}

/**
 * Get suite folders.
 *
 * Get testcase names found inside a suite.
 *
 * @param array $project
 *   Project details - key, name, path
 * @param string $suite
 *   Suite name
 *
 * @return array
 *   Testcase names including the folder name, if any.
 */
function _codeception_get_suite_testcases($project, $suite) {
  $tests = array();
  $path = $project['path'] . '/tests/' . $suite;
  $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
  foreach ($objects as $name => $object) {
    $filename = str_replace($path . '/', '', $name);
    if (preg_match('%Ce(p|s)t\.php$%', $filename)) {
      $tests[] = $filename;
    }
  }
  sort($tests);
  return $tests;
}

/**
 * Create root folder for Codeception projects.
 */
function _codeception_create_projects_root_folder() {
  if (!($codeception_path = variable_get('codeception_projects_path', ''))) {
    $codeception_path = variable_get('file_public_path', conf_path() . '/files') . '/codeception';
  }
  if (file_exists($codeception_path)) {
    return;
  }
  $codeception_dir = str_replace('/', DIRECTORY_SEPARATOR, DRUPAL_ROOT . '/' . $codeception_path);
  $replace = array(
    '@path' => $codeception_path,
    '@dir' => $codeception_dir,
  );
  mkdir($codeception_path);

  $file = format_string('@path/.htaccess', $replace);
  if (!file_exists($file)) {
    $htaccess = <<<EOD
deny from all
Options None
EOD;
    file_put_contents($file, $htaccess);
    chmod($file, 0644);
  }
}

/**
 * Delete a file or recursively delete a directory.
 *
 * @param string $str
 *   Path to file or directory
 */
function _codeception_recursive_delete($str) {
  if (is_file($str)) {
    return @unlink($str);
  }
  elseif (is_dir($str)) {
    $scan = glob(rtrim($str, '/') . '/*');
    foreach ($scan as $index => $path) {
      _codeception_recursive_delete($path);
    }
    return @rmdir($str);
  }
}

/**
 * Delete folder and its contents.
 *
 * @param string $path
 *   Path.
 */
function _codeception_delete_path($path) {
  foreach (new DirectoryIterator($path) as $file_info) {
    if (!$file_info->isDot()) {
      unlink($file_info->getPathname());
    }
  }
}

/**
 * Recursively copy a folder and its contents.
 *
 * @param string $source
 *   Source path.
 * @param string $dest
 *   Destination path.
 */
function _codeception_copy_folder_contents($source, $dest) {
  foreach ($iterator = new RecursiveIteratorIterator(
  new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST) as $item
  ) {
    if ($item->isDir()) {
      @mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
    }
    else {
      copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
    }
  }
}

/**
 * Get path to Codeception project.
 *
 * @param array $project
 *   Project parameters returned by codeception_get_codeception_project.
 *
 * @return string
 *   Path
 */
function _codeception_get_project_path($project) {
  if (!($codeception_path = variable_get('codeception_projects_path', ''))) {
    $codeception_path = variable_get('file_public_path', conf_path() . '/files') . '/codeception';
  }
  return $codeception_path . '/' . $project['key'];
}

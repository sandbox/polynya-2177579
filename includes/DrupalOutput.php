<?php

/**
 * @file
 * DrupalOutput
 */

namespace Codeception\Util\Console;

use Symfony\Component\Console\Output\StreamOutput;
use Symfony\Component\Console\Formatter\OutputFormatterInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Helper\TableHelper;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * DrupalOutput class
 */
class DrupalOutput extends StreamOutput implements ConsoleOutputInterface {

  protected $config = array(
    'colors' => FALSE,
    'verbosity' => self::VERBOSITY_NORMAL,
    'decorated' => TRUE,
  );

  /**
   * @var \Symfony\Component\Console\Helper\FormatterHelper
   */
  public $formatHelper;

  /**
   * Constructor.
   *
   * @param array $config
   *   Config.
   */
  public function __construct($config) {

    $this->config = array_merge($this->config, $config);

    $formatter = new OutputFormatter($this->config['colors']);

    $this->formatHelper = new FormatterHelper();

    $this->setFormatter($formatter);
  }

  /**
   * {@inheritdoc}
   */
  public function write($messages, $newline = FALSE, $type = self::OUTPUT_NORMAL) {
    $messages = (array) $messages;

    foreach ($messages as $message) {
      switch ($type) {
        case OutputInterface::OUTPUT_NORMAL:
          $message = $this->formatter->format($message);
          break;

        case OutputInterface::OUTPUT_RAW:
          break;

        case OutputInterface::OUTPUT_PLAIN:
          $message = strip_tags($this->formatter->format($message));
          break;

        default:
          throw new \InvalidArgumentException(sprintf('Unknown output type given (%s)', $type));
      }

      $this->doWrite($message, $newline);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function doWrite($message, $newline) {
    $cache = cache_get('codeception_log');
    $log = $cache->data;
    $log .= $message . ($newline ? PHP_EOL : '');
    cache_set('codeception_log', $log);
  }

  /**
   * {@inheritdoc}
   */
  public function getErrorOutput() {
    return $this->stderr;
  }

  /**
   * {@inheritdoc}
   */
  public function setErrorOutput(OutputInterface $error) {
    $this->stderr = $error;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormatter(OutputFormatterInterface $formatter) {
    $this->formatter = $formatter;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormatter() {
    return $this->formatter;
  }

  /**
   * Gets the decorated flag.
   *
   * @return bool
   *   True if the output will decorate messages, false otherwise.
   *
   * @api
   */
  public function isDecorated() {

  }

}

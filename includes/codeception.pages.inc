<?php

/**
 * @file
 * Codeception page callbacks
 */

/**
 * Report page for Codeception tests.
 *
 * @return string
 *   HTML for page
 */
function _codeception_report_page_callback() {
  $output = '';

  $projects = codeception_get_codeception_projects();
  $data = array();
  foreach ($projects as $project) {
    $data[] = l($project['name'], CODECEPTION_REPORTS_PATH . '/' . $project['key']);
    $items = array(
      'title' => 'Codeception projects',
      'items' => $data,
    );
  }
  $output .= theme('item_list', $items);
  return $output;
}

/**
 * Callback to execute Codeception 'run' command based on parameters.
 *
 * @param string $project_key
 *   Codeception project key
 * @param string $command
 *   Codeception command, only 'run' currently supported.
 * @param string $suite
 *   Codeception suite
 * @param string $folder
 *   Codeception folder
 * @param string $testcase
 *   Codeception testcase
 */
function _codeception_execute_callback($project_key, $command, $suite = NULL, $folder = NULL, $testcase = NULL, $data = NULL) {
  _codeception_create_projects_root_folder();
  switch ($command) {
    case 'run':
      if (variable_get('codeception_option_batch', TRUE)) {
        $batch = array(
          'operations' => array(),
          'finished' => '_codeception_batch_process_finished',
          'title' => t('Codeception batch'),
          'init_message' => t('Codeception batch is starting.'),
          'progress_message' => t('Processed @current out of @total.'),
          'error_message' => t('Codeception batch has encountered an error.'),
          'file' => drupal_get_path('module', 'codeception') . '/codeception.module',
        );

        $options = array(
          'project' => $project_key,
          'suite' => $suite,
          'folder' => $folder,
          'testcase' => $testcase,
          'data' => $data,
        );
        $batch['operations'][] = array('_codeception_batch_process', array($options));

        batch_set($batch);
        batch_process(CODECEPTION_REPORTS_PATH . '/' . $project_key);
      }
      else {
        _codeception_run($project_key, $suite, $folder, $testcase, $data);
      }
      break;

    case 'build':
      _codeception_build($project_key);
      break;

    default:
      drupal_set_message(t('Command "@comm" not supported', array(
        '@comm' => $command,
      )));
  }

  drupal_goto(CODECEPTION_REPORTS_PATH . '/' . $project_key);
}

/**
 * Callback for Codeception project page.
 *
 * @param string $project_key
 *   Project key
 *
 * @return string
 *   HTML
 */
function _codeception_project_page_callback($project_key) {
  $project_key = urldecode($project_key);
  $projects = codeception_get_codeception_projects();
  if ($project = $projects[$project_key]) {
    $output = theme('codeception_project_report', array('project' => $project));
  }
  else {
    $replace = array('@project' => $project_key);
    drupal_set_message(t('Project @project not found.', $replace), 'warning');
    drupal_goto(CODECEPTION_REPORTS_PATH);
  }
  if (user_access('execute codeception command')) {
    $output .= theme('codeception_project_actions', array('project' => $project));
  }
  return $output;
}

/**
 * Download a report file.
 *
 * Direct access to the _log directory is blocked by .htaccess so this callback
 * allows access to report.xml and report.html.
 *
 * @param string $project_key
 *   Project key
 * @param string $filetype
 *   File type, for example XML, HTML
 */
function _codeception_report_file_callback($project_key, $filetype) {
  if (in_array($filetype, array('xml', 'html', 'json', 'tap.log'))) {
    $project_key = urldecode($project_key);
    $projects = codeception_get_codeception_projects();
    if ($project = $projects[$project_key]) {
      $project_path = _codeception_get_project_path($project);
      $file_path = $project_path . '/tests/_log/report.' . $filetype;
      if (file_exists($file_path)) {
        $replace = array('!filetype' => $filetype);
        header(format_string('Content-Type: text/!filetype;', $replace));
        readfile($file_path);
        exit();
      }
    }
  }
  drupal_set_message(t('Report not found.'), 'warning');
  drupal_goto(CODECEPTION_REPORTS_PATH . '/' . urlencode($project_key));
}

<?php

/**
 * @file
 * DrupalExtension for Codeception.
 */

namespace Codeception\Extension;

use Codeception\Util\Console\DrupalOutput;

/**
 * DrupalExtension class.
 */
class DrupalExtension extends \Codeception\Platform\Extension {

  // Events to listen to.
  public static $events = array(
    'suite.before' => 'beforeSuite',
    'test.start' => 'startTest',
    'test.before' => 'beforeTest',
    'step.before' => 'beforeStep',
    'step.after' => 'afterStep',
    'step.fail' => 'failStep',
    'test.fail' => 'failTest',
    'test.error' => 'errorTest',
    'test.incomplete' => 'incompleteTest',
    'test.skipped' => 'skippedTest',
    'test.success' => 'successTest',
    'test.after' => 'afterTest',
    'test.end' => 'endTest',
    'suite.after' => 'afterSuite',
    'test.fail.print' => 'failTestPrint',
    'result.print.after' => 'afterResultPrint',
  );

  /**
   * Constructor.
   *
   * @param array $config
   *   Config
   * @param array $options
   *   Options.
   */
  public function __construct($config, $options) {
    if (isset($config['extensions']['config'][get_class($this)])) {
      $this->config = $config['extensions']['config'][get_class($this)];
    }
    $this->options = $options;
    $this->output = new DrupalOutput($options);
    $this->output->write('<doc>');
    $this->_reconfigure();
  }

  /**
   * React to beforeSuite event.
   *
   * @param \Codeception\Event\Suite $e
   *   Suite
   */
  public function beforeSuite(\Codeception\Event\Suite $e) {
    $suite = $e->getSuite();
    $this->output->write('<suite name="' . $suite->getName() . '">');
  }

  /**
   * React to startTest event.
   *
   * @param \Codeception\Event\Test $e
   *   Test
   */
  public function startTest(\Codeception\Event\Test $e) {
    $test = $e->getTest();
    $replace = array(
      '@filename' => $test->getFileName(),
    );
    $this->output->write(t('<test><file>@filename</file>', $replace));
  }

  /**
   * React to beforeTest event.
   *
   * @param \Codeception\Event\Test $e
   *   Test
   */
  public function beforeTest(\Codeception\Event\Test $e) {
    $this->output->write(t('<before_test/>'));
  }

  /**
   * React to beforeStep event.
   *
   * @param \Codeception\Event\Step $e
   *   Step
   */
  public function beforeStep(\Codeception\Event\Step $e) {
    $step = $e->getStep();
    $replace = array(
      '@action' => check_plain('I ' . $step->getHumanizedAction()),
    );
    $this->output->write(t('<step><action>@action</action>', $replace));
  }

  /**
   * React to afterStep event.
   *
   * @param \Codeception\Event\Step $e
   *   Step
   */
  public function afterStep(\Codeception\Event\Step $e) {
    $this->output->write('</step>');
  }

  /**
   * React to failStep event.
   *
   * @param \Codeception\Event\Fail $e
   *   Fail
   */
  public function failStep(\Codeception\Event\Fail $e) {
    $this->output->write('<fail_step/>');
  }

  /**
   * React to failTest event.
   *
   * @param \Codeception\Event\Fail $e
   *   Fail
   */
  public function failTest(\Codeception\Event\Fail $e) {
    $fail = $e->getFail();
    $this->output->write('<fail_test><code>' . $fail->getCode() . '</code><message>' . $fail->getMessage() . '</message><count>' . $e->getCount() . '</count></fail_test>');
  }

  /**
   * React to errorTest event.
   *
   * @param \Codeception\Event\Fail $e
   *   Fail
   */
  public function errorTest(\Codeception\Event\Fail $e) {
    $this->output->write('<error_test><fail>' . $e->getFail() . '</fail><count>' . $e->getCount() . '</count></error_test>');
  }

  /**
   * React to incompleteTest event.
   *
   * @param \Codeception\Event\Fail $e
   *   Fail
   */
  public function incompleteTest(\Codeception\Event\Fail $e) {
    $this->output->write('<incomplete_test><fail>' . $e->getFail() . '</fail><count>' . $e->getCount() . '</count></incomplete_test>');
  }

  /**
   * React to skippedTest event.
   *
   * @param \Codeception\Event\Fail $e
   *   Fail
   */
  public function skippedTest(\Codeception\Event\Fail $e) {
    $this->output->write('<skipped_test><fail>' . $e->getFail() . '</fail><count>' . $e->getCount() . '</count></skipped_test>');
  }

  /**
   * React to successTest event.
   *
   * @param \Codeception\Event\Test $e
   *   Test
   */
  public function successTest(\Codeception\Event\Test $e) {
    $this->output->write('<success_test/>');
  }

  /**
   * React to afterTest event.
   *
   * @param \Codeception\Event\Test $e
   *   Test
   */
  public function afterTest(\Codeception\Event\Test $e) {
    $this->output->write('<after_test/>');
  }

  /**
   * React to endTest event.
   *
   * @param \Codeception\Event\Test $e
   *   Test
   */
  public function endTest(\Codeception\Event\Test $e) {
    $this->output->write('</test>');
  }

  /**
   * React to afterSuite event.
   *
   * @param \Codeception\Event\Suite $e
   *   Suite
   */
  public function afterSuite(\Codeception\Event\Suite $e) {
    $this->output->write('</suite>');
  }

  /**
   * React to failTestPrint event.
   *
   * @param \Codeception\Event\Fail $e
   *   Fail
   */
  public function failTestPrint(\Codeception\Event\Fail $e) {
    $this->output->write('<fail_test_print/>');
  }

  /**
   * React to afterResultPrint event.
   *
   * @param \Codeception\Event\PrintResult $e
   *   PrintResult
   */
  public function afterResultPrint(\Codeception\Event\PrintResult $e) {
    $this->output->write('<after_result_print/></doc>');
  }

}

<?php

/**
 * @file
 * Functions for building a Codeception project.
 */

/**
 * Build a Codeception project.
 *
 * Copy the codeception project folder from its location declared in
 * hook_codeception_project to the working folder, by default in the public
 * files folder. Then replace tokens in yml files.
 */
function _codeception_build_project($project) {
  if (!is_array($project)) {
    return;
  }

  $from_dir = DRUPAL_ROOT . DIRECTORY_SEPARATOR . $project['path'];

  if (!($codeception_path = variable_get('codeception_projects_path', ''))) {
    $codeception_path = variable_get('file_public_path', conf_path() . '/files') . '/codeception';
  }

  $to_path = $codeception_path . '/' . $project['key'];
  $to_dir = DRUPAL_ROOT . '/' . $to_path;

  // Rebuild project folder.
  if (file_exists($to_dir)) {
    _codeception_recursive_delete($to_dir);
  }
  @mkdir($to_path);
  _codeception_copy_folder_contents($from_dir, $to_dir);

  // Replace tokens in YML files.
  _codeception_build_yml($to_path . '/codeception.yml');
  $files = scandir($to_path . '/tests');
  if (is_array($files)) {
    foreach ($files as $file) {
      $filepath = $to_path . '/tests/' . $file;
      $extension = pathinfo($filepath, PATHINFO_EXTENSION);
      if ($extension == 'yml') {
        _codeception_build_yml($filepath);
      }
    }
  }
}

/**
 * Build yml file from template.
 *
 * @param string $file
 *   File path.
 *
 * @return bool
 *   TRUE for success.
 */
function _codeception_build_yml($file, $callback = NULL) {
  $options = array(
    'sanitize' => FALSE,
  );
  if ($callback) {
    $options['callback'] = $callback;
  }
  if ($template = file_get_contents($file)) {
    $output = token_replace($template, array(), $options);
    if (file_put_contents($file, $output) !== FALSE) {
      return TRUE;
    }
  }
  return FALSE;
}

<?php

/**
 * @file
 * Codeception admin form.
 */

/**
 * Configuration page for Codeception tests.
 *
 * @return string
 *   HTML for page
 */
function codeception_admin_form($form, &$form_state) {

  $form['codeception_rebuild_project'] = array(
    '#type' => 'checkbox',
    '#title' => t('Rebuild project'),
    '#default_value' => variable_get('codeception_rebuild_project', TRUE),
    '#description' => t('Always rebuild the Codeception project folder. Only required during development when changes are made to the project template.'),
  );
  $form['codeception_execute_build'] = array(
    '#type' => 'checkbox',
    '#title' => t('Execute "build" before "run"'),
    '#default_value' => variable_get('codeception_execute_build', TRUE),
    '#description' => t('Should only be necessary if modules have been changed in yml files or new methods have been added to helpers.'),
  );
  $form['codeception_option_batch'] = array(
    '#type' => 'checkbox',
    '#title' => t('Batch'),
    '#default_value' => variable_get('codeception_option_batch', TRUE),
    '#description' => t('Always run Codeception as a batch'),
  );
  $form['codeception_require_autoload'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autoload'),
    '#default_value' => variable_get('codeception_require_autoload', TRUE),
    '#description' => t('Require Codeception autoload.php. Uncheck if using Composer Autoload module.'),
  );
  $form['codeception_option_output'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verbose output'),
    '#default_value' => variable_get('codeception_option_output', TRUE),
    '#description' => t('Codeception output flag'),
  );
  $form['codeception_yml_drupalextension'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable token [codeception:drupalextension]'),
    '#default_value' => variable_get('codeception_yml_drupalextension', TRUE),
    '#description' => t('Used to include the extension DrupalExtension in codeception.yml. The extension is required to get XML returned to Drupal.'),
  );
  $form['codeception_yml_extensions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include "Extensions" line in token [codeception:drupalextension]'),
    '#default_value' => variable_get('codeception_yml_extensions', TRUE),
    '#description' => t('Flag has no effect if the token [codeception:drupalextension] is not enabled'),
  );
  $form['codeception_option_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug option'),
    '#default_value' => variable_get('codeception_option_debug', FALSE),
    '#description' => t('Codeception --debug command option'),
  );
  $form['codeception_option_silent'] = array(
    '#type' => 'checkbox',
    '#title' => t('Silent option'),
    '#default_value' => variable_get('codeception_option_silent', FALSE),
    '#description' => t('Codeception --debug command option'),
  );
  $form['codeception_option_steps'] = array(
    '#type' => 'checkbox',
    '#title' => t('Steps option'),
    '#default_value' => variable_get('codeception_option_steps', FALSE),
    '#description' => t('Codeception --steps command option'),
  );
  $form['files'] = array(
    '#type' => 'fieldset',
    '#title' => t('Report files'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['files']['codeception_option_xml'] = array(
    '#type' => 'checkbox',
    '#title' => t('Generate report in XML format'),
    '#default_value' => variable_get('codeception_option_xml', TRUE),
    '#description' => t('Codeception --xml command option'),
  );

  $form['files']['codeception_option_html'] = array(
    '#type' => 'checkbox',
    '#title' => t('HTML option'),
    '#default_value' => variable_get('codeception_option_html', TRUE),
    '#description' => t('Generate report in HTML format'),
  );

  $form['files']['codeception_option_tap'] = array(
    '#type' => 'checkbox',
    '#title' => t('Generate report in TAP format'),
    '#default_value' => variable_get('codeception_option_tap', FALSE),
    '#description' => t('Codeception --tap command option'),
  );

  $form['files']['codeception_option_json'] = array(
    '#type' => 'checkbox',
    '#title' => t('Generate report in JSON format'),
    '#default_value' => variable_get('codeception_option_json', FALSE),
    '#description' => t('Codeception --json command option'),
  );

  return system_settings_form($form);
}

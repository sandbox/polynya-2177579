<?php

/**
 * @file
 * Defines drush commands for Codeception.
 */

/**
 * Implements hook_drush_help().
 */
function codeception_drush_help($section) {
  switch ($section) {
    case 'drush:codeception':
      return dt('Execute Codeception "run" command.');
  }
}

/**
 * Implements hook_drush_command().
 */
function codeception_drush_command() {
  return array(
    'codeception' => array(
      'description' => dt('Execute Codeception command.'),
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      'examples' => array('drush codeception run PROJECT SUITE FOLDER TESTCASE' => 'Execute Codeception "run" command with parameters'),
      'options' => array(
        'project' => array(
          'description' => 'Project key',
          'value' => 'required',
        ),
        'command' => array(
          'description' => 'Codeception command, for example run',
          'value' => 'required',
        ),
        'suite' => array(
          'description' => 'Suite',
          'value' => 'optional',
        ),
        'folder' => array(
          'description' => 'Folder',
          'value' => 'optional',
        ),
        'testcase' => array(
          'description' => 'Test case',
          'value' => 'optional',
        ),
      ),
    ),
  );
}

/**
 * Implements drush_COMMANDFILE_COMMANDNAME().
 */
function drush_codeception($command, $project_key, $suite = NULL, $folder = NULL, $testcase = NULL) {
  $replace = array(
    '@project' => $project_key,
    '@command' => $command,
    '@suite' => $suite,
    '@folder' => $folder,
    '@testcase' => $testcase,
  );
  drush_print(t('Running Codeception'));
  drush_print(t('Command "@command"', $replace));
  drush_print(t('Project "@project"', $replace));

  switch (strtolower($command)) {
    case 'run':
      drush_print(t('Suite "@suite"', $replace));
      drush_print(t('Folder "@folder"', $replace));
      drush_print(t('Testcase "@testcase"', $replace));
      drush_print(_codeception_run($project_key, $suite, $folder, $testcase));
      break;

    case 'build':
      drush_print(_codeception_build($project_key));
      break;

    default:
      drush_print(t('Command @command is not recognised.', $replace));
  }
  drush_print(t('Codeception finished'));
}

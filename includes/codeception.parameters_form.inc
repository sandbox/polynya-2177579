<?php

/**
 * @file
 * Codeception parameters form.
 */

/**
 * Set parameters to be available in Codeception tests.
 *
 * Parameters can be accessed in Codeception tests in the global variable
 * $_codeception_drupal_parameters
 *
 * @param string $project_id
 *   Project id
 *
 * @see codeception_parameters_form_validate()
 * @see codeception_parameters_form_submit()
 *
 * @ingroup forms
 */
function codeception_parameters_form($form, &$form_state, $project_id) {

  $parameters = variable_get('codeception_parameters_' . $project_id, array());
  foreach ($parameters as $key => $value) {
    $parameters_string .= $key . ',' . $value;
  }

  $form['info'] = array(
    '#markup' => t('Define parameters that can be accessed in Codeception tests in the global $_codeception_drupal_parameters'),
  );
  $replace = array('@param' => '$_codeception_drupal_parameters[\'nid\']');
  $form['parameters'] = array(
    '#type' => 'textarea',
    '#title' => t('Parameters'),
    '#default_value' => $parameters_string,
    '#description' => t('Put one parameter on each line as key and value separated by a comma. For example nid,1234 defines parameter @param with value 1234.', $replace),
  );
  $form['project_id'] = array(
    '#type' => 'hidden',
    '#value' => $project_id,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save parameters',
  );

  return $form;
}

/**
 * Form validation handler for codeception_parameters_form().
 *
 * @see codeception_parameters_form_submit()
 */
function codeception_parameters_form_validate($form, &$form_state) {
  $parameters_string = $form_state['values']['parameters'];
  $lines = explode(PHP_EOL, $parameters_string);
  foreach ($lines as $line) {
    if (substr_count($line, ',') != 1) {
      form_set_error('parameters', t("Each line must contain 1 comma to separate the parameter's key and value."));
      return;
    }
  }
}

/**
 * Form submission handler for codeception_parameters_form().
 *
 * @see codeception_parameters_form_validate()
 */
function codeception_parameters_form_submit($form, &$form_state) {
  $project_id = $form_state['values']['project_id'];
  $parameters_string = $form_state['values']['parameters'];
  $lines = explode(PHP_EOL, $parameters_string);
  foreach ($lines as $line) {
    list($key, $value) = explode(',', $line);
    $params[$key] = $value;
  }
  variable_set('codeception_parameters_' . $project_id, $params);
}
